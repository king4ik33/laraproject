FROM php:8.0.7-fpm

RUN apt-get update && apt-get install -y \
    zip \
    libzip-dev \
    supervisor \
    nano \
    && \
    docker-php-ext-install bcmath sockets zip pdo_mysql \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug

# ---- COMPOSER ----
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
