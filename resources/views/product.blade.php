@extends('layouts.master')

@section('title', 'Товар')

@section('content')
    <h1>{{ $product }}</h1>
    <p>Цена: <b>71990 руб.</b></p>
    <p>Отличный телефон</p>
    <a href="" class="btn btn-success">Добавить в корзину</a>
@endsection
